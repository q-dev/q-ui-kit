import { Colors } from './colors';

import 'styled-components';

declare module 'styled-components' {
  export interface DefaultTheme {
    currentTheme: string;
    isDarkTheme: boolean;
    colors: Colors;
    onChangeTheme: () => void;
  }
}
