export type FieldOption<T> = {
  value: T
  label: string
}
export type FieldOptions<T> = FieldOption<T>[];
export type RadioOptions<T> = (FieldOption<T> & { tip?: string, disabled?: boolean })[]
