import { BigNumber } from 'bignumber.js';

export function toBigNumber (value: BigNumber.Value): BigNumber {
  return new BigNumber(value);
}

export function formatNumber (value: BigNumber.Value, precision = 4): string {
  return new BigNumber(value).decimalPlaces(precision, BigNumber.ROUND_DOWN).toFormat();
}

export function formatPercent (value: BigNumber.Value): string {
  const percent = Number(formatNumber(value));
  return isNaN(Number(percent)) ? '0 %' : `${formatNumber(value)} %`;
}

export function parseNumber (value: string): number {
  return Number(value.toString().replace(/[Q,%]/g, ''));
}
