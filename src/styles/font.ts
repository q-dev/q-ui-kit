import { createGlobalStyle } from 'styled-components';

import LoraRegular from '@/assets/fonts/Lora/Lora-Regular.ttf';
import LoraSemiBold from '@/assets/fonts/Lora/Lora-SemiBold.ttf';
import OpenSansLight from '@/assets/fonts/OpenSans/OpenSans-Light.ttf';
import OpenSansRegular from '@/assets/fonts/OpenSans/OpenSans-Regular.ttf';
import OpenSansSemiBold from '@/assets/fonts/OpenSans/OpenSans-SemiBold.ttf';
import QIcons from '@/assets/fonts/Q-Icons/Q-Icons.ttf';

export const FontStyle = createGlobalStyle`
  /* Open Sans */
  @font-face {
    font-family: OpenSans;
    src: url(${OpenSansLight});
    font-style: normal;
    font-weight: 300;
  }

  @font-face {
    font-family: OpenSans;
    src: url(${OpenSansRegular});
    font-style: normal;
    font-weight: 400;
  }

  @font-face {
    font-family: OpenSans;
    src: url(${OpenSansSemiBold});
    font-style: normal;
    font-weight: 600;
  }

  /* Lora */
  @font-face {
    font-family: Lora;
    src: url(${LoraRegular});
    font-style: normal;
    font-weight: 400;
  }

  @font-face {
    font-family: Lora;
    src: url(${LoraSemiBold});
    font-style: normal;
    font-weight: 600;
  }

  /* Q Icons */
  @font-face {
    font-family: Q-Icons;
    src: url(${QIcons});
    font-weight: normal;
    font-style: normal;
    font-display: block;
  }
`;
