import { useState } from 'react';
import { registerLocale } from 'react-datepicker';

import datePickerEn from 'date-fns/locale/en-GB';

import Calendar from '@/components/Calendar';

registerLocale('en-GB', datePickerEn);

function Calendars () {
  const [date, setDate] = useState(new Date());

  return (
    <div className="block">
      <h2 className="text-h2">Calendars</h2>
      <div className="block-content">
        <div className="input-list">
          <Calendar
            locale="en-GB"
            label="Regular calendar"
            placeholder="Type some date"
            value={date}
            onChange={setDate}
          />

          <Calendar
            disabled
            locale="en-GB"
            label="Disabled calendar"
            placeholder="Type some date"
            value={date}
            onChange={setDate}
          />
        </div>
      </div>
    </div>
  );
}

export default Calendars;
