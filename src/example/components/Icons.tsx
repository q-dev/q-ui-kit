import Icon from '@/components/Icon';
import icons from '@/components/Icon/icons.json';
import Tooltip from '@/components/Tooltip';

function Icons () {
  return (
    <div className="block">
      <h2 className="text-h2">Icons</h2>
      <div className="block-content">
        <div className="icon-list">
          {Object.keys(icons).map((icon) => (
            <Tooltip
              key={icon}
              trigger={(
                <Icon
                  className="icon"
                  name={icon as keyof typeof icons}
                />
              )}
            >
              {icon}
            </Tooltip>
          ))}
        </div>
      </div>
    </div>
  );
}

export default Icons;
