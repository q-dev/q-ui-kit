import { HTMLAttributes, ReactNode } from 'react';

import { motion, useMotionValue, useTransform } from 'framer-motion';
import uniqueId from 'lodash/uniqueId';

import { CheckContainer } from './styles';

interface Props extends Omit<HTMLAttributes<HTMLDivElement>, 'onChange'> {
  value: boolean
  label?: ReactNode
  disabled?: boolean
  onChange: (value: boolean) => void
};

function Check ({
  value,
  label,
  disabled = false,
  onChange,
  className,
  ...rest
}: Props) {
  const inputId = `q-ui-check-${uniqueId()}`;

  const pathLength = useMotionValue<number>(0);
  const opacity = useTransform(pathLength, [0.05, 0.15], [0, 1]);

  return (
    <CheckContainer
      className={`q-ui-check ${className || ''}`}
      $checked={value}
      $disabled={disabled}
      {...rest}
    >
      <input
        id={inputId}
        className="q-ui-check__input"
        type="checkbox"
        checked={value}
        disabled={disabled}
        onChange={() => onChange(!value)}
      />

      <motion.div className="q-ui-check__frame">
        <svg
          className="q-ui-check__icon"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="15 20 120 120"
        >
          <motion.path
            d="M38 74.707l24.647 24.646L116.5 45.5"
            fill="transparent"
            className="q-ui-check__path"
            strokeWidth="20"
            strokeLinejoin="round"
            strokeLinecap="round"
            animate={{ pathLength: value ? 0.9 : 0 }}
            style={{ pathLength, opacity }}
          />
        </svg>
      </motion.div>

      <label
        htmlFor={inputId}
        className="q-ui-check__label text-md"
      >
        {label}
      </label>
    </CheckContainer>
  );
}

export default Check;
