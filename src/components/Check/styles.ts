import styled, { css } from 'styled-components';

export const CheckContainer = styled.div<{
  $checked: boolean
  $disabled: boolean
}>`
  position: relative;
  display: flex;
  gap: 4px;
  align-items: center;

  .q-ui-check__input {
    cursor: pointer;
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    opacity: 0;
    z-index: 1;

    &:disabled {
      cursor: not-allowed;
    }
  }

  .q-ui-check__frame {
    position: relative;
    border-radius: 2px;
    width: 16px;
    height: 16px;
    transition: all 150ms ease-out;
    border: 2px solid ${({ theme, $checked }) => $checked
      ? 'transparent'
      : theme.colors.iconPrimary
    };
    background-color: ${({ theme, $checked }) => $checked
      ? theme.colors.primaryMain
      : 'transparent'
    };

    ${({ theme, $disabled, $checked }) => $disabled && css`
      background-color: ${$checked
        ? theme.colors.disableSecondary
        : 'transparent'
      };
      border-color: ${$checked
        ? 'transparent'
        : theme.colors.disableSecondary
      };
    `}
  }

  ${({ theme, $checked, $disabled }) => !$disabled && css`
    &:hover .q-ui-check__frame {
      border-color: ${$checked
        ? 'transparent'
        : theme.colors.iconAdditional
      };
      background-color: ${$checked
        ? theme.colors.primaryMiddle
        : 'transparent'
      };
    }
  `}

  .q-ui-check__input:focus-visible ~ .q-ui-check__frame {
    outline: 2px solid ${({ theme }) => theme.colors.primaryLight};
  }

  .q-ui-check__icon {
    position: absolute;
  }

  .q-ui-check__path {
    stroke: ${({ theme }) => theme.colors.backgroundPrimary};
  }

  .q-ui-check__label {
    color: ${({ theme, $disabled }) => $disabled
      ? theme.colors.disableSecondary
      : theme.colors.textPrimary
    };
  }
`;
