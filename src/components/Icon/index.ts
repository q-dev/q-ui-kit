import icons from './icons.json';

export { default } from './Icon';
export type IconName = keyof typeof icons
