import { ChangeEvent, TextareaHTMLAttributes } from 'react';

import uniqueId from 'lodash/uniqueId';

import { TextareaWrapper } from './styles';

type TextareaProps = TextareaHTMLAttributes<HTMLTextAreaElement>
interface Props extends Omit<TextareaProps, 'onChange' | 'prefix' | 'value'> {
  value: string | number | boolean
  label?: string
  error?: string
  hint?: string
  disabled?: boolean
  maxLength?: number
  rows?: number
  cols?: number
  decimals?: number;
  onChange: (val: string) => void
}

function Textarea ({
  value,
  label,
  error,
  disabled,
  hint,
  maxLength,
  rows,
  cols,
  className,
  onChange = () => { },
  ...rest
}: Props) {
  const textareaId = `q-ui-textarea__${uniqueId()}`;

  const currentLength = String(value).length;

  const handleChange = (e: ChangeEvent) => {
    const value = (e.target as HTMLTextAreaElement).value;
    if (maxLength && value.length > maxLength) {
      return;
    }
    onChange(value);
  };

  return (
    <TextareaWrapper
      className={`q-ui-textarea ${className || ''}`}
      $error={error}
      $disabled={disabled}
    >
      {label && (
        <div className="q-ui-textarea__label-wrp">
          <label
            htmlFor={textareaId}
            className="q-ui-textarea__label text-md"
          >
            {label}
          </label>
          {maxLength && (
            <span className="q-ui-textarea__label-counter text-sm font-light">
              {`${currentLength}/${maxLength}`}
            </span>
          )}
        </div>
      )}

      <div className="q-ui-textarea__container">
        <textarea
          id={textareaId}
          className="q-ui-textarea__textarea text-md"
          value={String(value)}
          disabled={disabled}
          maxLength={maxLength}
          rows={rows}
          cols={cols}
          onChange={handleChange}
          {...rest}
        />
      </div>

      {error && (
        <span className="q-ui-textarea__error text-md font-light">{error}</span>
      )}

      {hint && !error && (
        <span className="q-ui-textarea__hint text-md font-light">{hint}</span>
      )}
    </TextareaWrapper>
  );
};

export default Textarea;
