import { HTMLAttributes } from 'react';

import Bulb from './components/Bulb';
import EmptyList from './components/EmptyList';

type IllustrationType = 'bulb' | 'empty-list';
interface Props extends HTMLAttributes<SVGAElement> {
  type: IllustrationType
}

function Illustration ({ type, className, ...rest }: Props) {
  const illustrationsMap: Record<IllustrationType, JSX.Element> = {
    bulb: <Bulb className={`q-ui-illustration ${className || ''}`} {...rest} />,
    'empty-list': <EmptyList className={`q-ui-illustration ${className || ''}`} {...rest} />,
  };

  return illustrationsMap[type];
}

export default Illustration;
