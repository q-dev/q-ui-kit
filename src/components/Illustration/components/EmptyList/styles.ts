import styled from 'styled-components';

export const StyledEmptyList = styled.svg`
  .q-ui-illustration-empty-list__bg {
    fill: ${({ theme }) => theme.colors.listBackgroundPrimary};
  }

  .q-ui-illustration-empty-list__item-bg {
    fill: ${({ theme }) => theme.colors.listBackgroundSecondary};
  }

  .q-ui-illustration-empty-list__item-primary {
    fill: ${({ theme }) => theme.colors.listPrimary};
  }

  .q-ui-illustration-empty-list__item-secondary {
    fill: ${({ theme }) => theme.colors.listSecondary};
  }
`;
