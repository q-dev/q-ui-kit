import styled from 'styled-components';

export const StyledBulb = styled.svg`
  .q-ui-illustration-bulb__bg {
    fill: ${({ theme }) => theme.colors.bulbBackground};
  }

  .q-ui-illustration-bulb__circle {
    fill: ${({ theme }) => theme.colors.bulbBackground};
  }

  .q-ui-illustration-bulb__gradient-a {
    stop-color: ${({ theme }) => theme.colors.bulbGradientA};
  }

  .q-ui-illustration-bulb__gradient-b {
    stop-color: ${({ theme }) => theme.colors.bulbGradientB};
  }

  .q-ui-illustration-bulb__inner-dark {
    fill: ${({ theme }) => theme.colors.bulbInnerDark};
  }

  .q-ui-illustration-bulb__inner-medium {
    fill: ${({ theme }) => theme.colors.bulbInnerMedium};
  }

  .q-ui-illustration-bulb__inner-stroke-medium {
    stroke: ${({ theme }) => theme.colors.bulbInnerMedium};
  }

  .q-ui-illustration-bulb__inner-stroke-light {
    stroke: ${({ theme }) => theme.colors.bulbInnerLight};
  }
`;
