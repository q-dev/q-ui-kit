import { HTMLAttributes, useEffect, useState } from 'react';

import { SelectContainer } from './styles';

import Button from '@/components/Button';
import Dropdown from '@/components/Dropdown';
import Icon from '@/components/Icon';
import Input from '@/components/Input';
import { FieldOptions } from '@/types/field';

type ValueType = number | string | boolean
interface Props<T extends ValueType> extends Omit<HTMLAttributes<HTMLDivElement>, 'onChange'> {
  value: string | number | boolean
  options: FieldOptions<T>
  label?: string
  error?: string
  disabled?: boolean
  chips?: boolean
  combobox?: boolean
  placeholder?: string
  hint?: string
  onChange: (val: T) => void
}

function Select<T extends ValueType> ({
  value,
  options,
  label,
  error,
  chips = false,
  disabled = false,
  combobox = false,
  placeholder,
  hint,
  onChange,
  className,
  ...rest
}: Props<T>) {
  const [filter, setFilter] = useState('');
  const [open, setOpen] = useState(false);

  const selectedOption = options.find(option => value && option.value === value);
  const filteredOptions = options.filter(option => option.label.toLowerCase().includes(filter.toLowerCase()));

  useEffect(() => {
    setFilter(combobox ? String(value) : '');
  }, [value, open]);

  const selectOption = (val: T) => {
    onChange(val);
    setOpen(false);
    setFilter(combobox ? String(val) : '');
  };

  const handleFilterChange = (val: string) => {
    setFilter(val);
    if (combobox) {
      onChange(val as T);
    }
  };

  const isOptionsShown = filteredOptions.length > 0 || !combobox;
  const selectTrigger = chips && !combobox
    ? (
      <Button
        compact
        className="q-ui-select__chips"
        disabled={disabled}
        look={value ? 'primary' : 'secondary'}
      >
        {Boolean(value) && <Icon name="check" />}
        <span>{selectedOption?.label || placeholder}</span>
        <Icon className="q-ui-select__icon" name="arrow-down-small" />
      </Button>
    )
    : (
      <Input
        value={open || combobox ? filter : selectedOption?.label || ''}
        label={label}
        placeholder={open ? selectedOption?.label || placeholder : placeholder}
        disabled={disabled}
        onClick={() => setOpen(true)}
        onFocus={() => setOpen(true)}
        onChange={handleFilterChange}
      >
        {isOptionsShown && (
          <button
            className="q-ui-select__arrow"
            type="button"
            disabled={disabled}
            onClick={() => setOpen(!open)}
          >
            <Icon
              className="q-ui-select__icon"
              name="expand-more"
            />
          </button>
        )}
      </Input>
    );

  return (
    <SelectContainer
      className={`q-ui-select ${className || ''}`}
      $open={open}
      $disabled={disabled}
      {...rest}
    >
      <Dropdown
        open={open}
        fullWidth={!chips}
        disabled={chips ? disabled : true}
        trigger={selectTrigger}
        onToggle={setOpen}
      >
        {isOptionsShown && (
          <div className="q-ui-select__options">
            {filteredOptions.map((option) => (
              <button
                key={String(option.value)}
                type="button"
                className={`q-ui-select__option break-word text-md ${value === option.value ? 'q-ui-select__option--active' : ''}`}
                onClick={() => selectOption(option.value)}
              >
                <Icon
                  name="check"
                  className={`q-ui-select__option-icon ${value === option.value ? 'q-ui-select__option-icon--active' : ''}`}
                />
                <span>{option.label}</span>
              </button>
            ))}

            {filteredOptions.length === 0 && (
              <p className="q-ui-select__stub text-md">
                No options found
              </p>
            )}
          </div>
        )}
      </Dropdown>

      {error && (
        <span className="q-ui-select__error text-md font-light">{error}</span>
      )}

      {hint && !error && (
        <span className="q-ui-select__hint text-md font-light">{hint}</span>
      )}
    </SelectContainer>
  );
};

export default Select;
