import { HTMLAttributes } from 'react';

import { AnimateSharedLayout, motion } from 'framer-motion';

import { SegmentedButtonContainer } from './styles';

import { FieldOptions } from '@/types/field';

type ValueType = number | string | boolean;
interface Props<T extends ValueType> extends Omit<HTMLAttributes<HTMLDivElement>, 'value' | 'onChange'> {
  value: T;
  light?: boolean;
  options?: FieldOptions<T>;
  onChange?: (value: T) => void;
}

function SegmentedButton<T extends ValueType> ({
  value,
  light = false,
  options = [],
  className,
  onChange = () => {},
  ...rest
}: Props<T>) {
  const handleChange = (selected: T) => {
    if (selected !== value) {
      onChange(selected);
    }
  };

  return (
    <SegmentedButtonContainer
      className={`q-ui-segmented-button ${className || ''}`}
      $light={light}
      {...rest}
    >
      <AnimateSharedLayout>
        {options.map((option) => (
          <button
            key={String(option.value)}
            className={`q-ui-segmented-button__item text-md ${value === option.value ? 'q-ui-segmented-button__item--active' : ''}`}
            type="button"
            onClick={() => handleChange(option.value)}
          >
            <span className="q-ui-segmented-button__item-lbl">
              {option.label}
            </span>
            {value === option.value && (
              <motion.div layoutId="segmented-bg" className="q-ui-segmented-button__item-active" />
            )}
          </button>
        ))}
      </AnimateSharedLayout>
    </SegmentedButtonContainer>
  );
};

export default SegmentedButton;
