export { default } from './Tip';
export type TipType = 'info' | 'warning';
