import { HTMLAttributes } from 'react';

import { RadioGroupContainer } from './styles';

import Radio from '@/components/Radio';
import { RadioOptions } from '@/types/field';

type ValueType = number | string | boolean
interface Props<T extends ValueType> extends Omit<HTMLAttributes<HTMLDivElement>, 'onChange'> {
  value: T
  name: string
  options?: RadioOptions<T>
  extended?: boolean
  disabled?: boolean
  label?: string
  error?: string
  row?: boolean
  onChange: (value: T) => void
};

function RadioGroup<T extends ValueType> ({
  name,
  value,
  options = [],
  label = '',
  error,
  row = false,
  extended = false,
  disabled = false,
  onChange,
  className,
  ...rest
}: Props<T>) {
  return (
    <RadioGroupContainer
      className={`q-ui-radio-group ${className || ''}`}
      $row={row}
      $extended={extended}
      $disabled={disabled}
      {...rest}
    >
      {label && <p className="q-ui-radio-group__lbl text-md">{label}</p>}

      <div className="q-ui-radio-group__options">
        {options.map((option) => (
          <Radio
            key={String(option.value)}
            className="q-ui-radio-group__option"
            label={option.label}
            name={name}
            value={value}
            checked={option.value === value}
            tip={option.tip}
            extended={extended}
            disabled={option?.disabled || disabled}
            onChange={() => onChange(option.value)}
          />
        ))}
      </div>

      {error && (
        <span className="q-ui-radio-group__error text-md font-light">
          {error}
        </span>
      )}
    </RadioGroupContainer>
  );
}

export default RadioGroup;
