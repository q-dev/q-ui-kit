
import styled, { css } from 'styled-components';

import { media } from '@/styles/media';

export const RadioGroupContainer = styled.div<{
  $row: boolean
  $disabled: boolean
  $extended: boolean
}>`
  .q-ui-radio-group__lbl {
    margin-bottom: 8px;
    color: ${({ theme, $disabled }) => $disabled
      ? theme.colors.disableSecondary
      : theme.colors.textSecondary
    };
  }

  .q-ui-radio-group__options {
    display: flex;
    flex-direction: ${({ $row }) => $row ? 'row' : 'column'};
    gap: 12px;

    ${({ $extended }) => $extended && css`
      display: grid;
      grid-template-columns: repeat(2, 1fr);
      gap: 16px;

      ${media.lessThan('medium')} {
        grid-template-columns: 1fr;
      }
    `};
  }

  .q-ui-radio-group__error {
    margin-top: 4px;
    color: ${({ theme }) => theme.colors.errorMain};
  }
`;
