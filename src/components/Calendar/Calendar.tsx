import DatePicker, { ReactDatePickerProps } from 'react-datepicker';

import { CalendarWrapper } from './styles';

interface Props extends Omit<ReactDatePickerProps, 'value'> {
  value: Date | null;
  label?: string;
  placeholder?: string;
  error?: string;
  disabled?: boolean;
  onChange: (val: Date) => void;
  locale: string;
}

function Calendar ({
  value,
  label,
  error,
  placeholder,
  disabled = false,
  onChange,
  locale,
  className,
  ...rest
}: Props) {
  return (
    <CalendarWrapper
      className={`q-ui-calendar ${className || ''}`}
      $error={error}
      $disabled={disabled}
    >
      {label && <p className="q-ui-calendar__lbl text-md">{label}</p>}

      <DatePicker
        showTimeSelect
        locale={locale}
        selected={value}
        dateFormat="MMMM d, yyyy, h:mm a"
        filterTime={(d) => new Date(d).getTime() > Date.now()}
        timeFormat="HH:mm"
        timeIntervals={1}
        disabled={disabled}
        placeholderText={placeholder}
        {...rest}
        onChange={onChange}
      />

      {error && <span className="q-ui-calendar__error text-md font-light">{error}</span>}
    </CalendarWrapper>
  );
}

export default Calendar;
