# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Changed
- Repository documentation

## [0.8.3] 2023-05-11
### Added
- Separator for number in `Input`

## [0.8.2]
### Fixed
- Select hint

## [0.8.1]
### Fixed
- Empty list Illustration

## [0.8.0]
### Added
- Textarea field

### Changed
- RadioGroup field options

## [0.7.0]
### Added
- Color scheme
- Custom color in provider

### Changed
- Class namespaces on components
- Colors in UI components
- Current light colors 

### Removed
- color.ts in UI components

## [0.6.0] - 2022-10-17
### Removed
- MDI font

## [0.5.1] - 2022-10-14
### Fixed
- Import mdi style

## [0.5.0] - 2022-10-14
### Added
- Icons

### Changed
- Input field
- Segment buttons

### Fixed
- Spinner animation
- Range percentage

## [0.4.1] - 2022-10-05
### Fixed
- Font style

## [0.4.0] - 2022-09-09
### Added
- Example

## [0.3.0] - 2022-09-09
### Added
- Font styles

## [0.2.3] - 2022-09-09
### Fixed
- Calendar styles
- Style provider props

## [0.2.2] - 2022-09-08
### Changed
- Props on Range

## [0.2.1] - 2022-09-08
### Changed
- `framer-motion` version

## [0.2.0] - 2022-09-05
### Added
- Components
- Hooks
- Providers
- Styles
- Types
- Utils

## 0.1.0 - 2022-09-02
### Added
- Initiated project

[Unreleased]: https://gitlab.com/q-dev/front-end-tools/q-ui-kit/compare/v0.6.1...main
[0.6.1]: https://gitlab.com/q-dev/front-end-tools/q-ui-kit/compare/v0.6.0...v0.6.1
[0.6.0]: https://gitlab.com/q-dev/front-end-tools/q-ui-kit/compare/v0.5.1...v0.6.0
[0.5.1]: https://gitlab.com/q-dev/front-end-tools/q-ui-kit/compare/v0.5.0...v0.5.1
[0.5.0]: https://gitlab.com/q-dev/front-end-tools/q-ui-kit/compare/v0.4.1...v0.5.0
[0.4.1]: https://gitlab.com/q-dev/front-end-tools/q-ui-kit/compare/v0.4.0...v0.4.1
[0.4.0]: https://gitlab.com/q-dev/front-end-tools/q-ui-kit/compare/v0.3.0...v0.4.0
[0.3.0]: https://gitlab.com/q-dev/front-end-tools/q-ui-kit/compare/v0.2.3...v0.3.0
[0.2.3]: https://gitlab.com/q-dev/front-end-tools/q-ui-kit/compare/v0.2.2...v0.2.3
[0.2.2]: https://gitlab.com/q-dev/front-end-tools/q-ui-kit/compare/v0.2.1...v0.2.2
[0.2.1]: https://gitlab.com/q-dev/front-end-tools/q-ui-kit/compare/v0.2.0...v0.2.1
[0.2.0]: https://gitlab.com/q-dev/front-end-tools/q-ui-kit/compare/v0.1.0...v0.2.0
